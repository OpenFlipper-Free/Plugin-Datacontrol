
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <DataControlPlugin.hh>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(DataControl, m) {

  QObject* pluginPointer = getPluginPointer("DataControl");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-DataControl" << std::endl;
     return;
   }

  DataControlPlugin* plugin = qobject_cast<DataControlPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-DataControl" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< DataControlPlugin,std::unique_ptr<DataControlPlugin, py::nodelete> > data(m, "DataControl");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  data.def(py::init([plugin]() { return plugin; }));


  data.def("getObjectName",  &DataControlPlugin::getObjectName,
                                 QCoreApplication::translate("PythonDocDataControl","Returns the name of an object with given id.").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data()) );


  data.def("objectDelete",  &DataControlPlugin::objectDelete,
                                 QCoreApplication::translate("PythonDocDataControl","Delete the given object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data()));

  data.def("copyObject",   static_cast<int (DataControlPlugin::*)(int)>( &DataControlPlugin::copyObject),
                                 QCoreApplication::translate("PythonDocDataControl","Create a copy of an object and return the id of the new object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object to copy").toLatin1().data()));

  data.def("getObject",  &DataControlPlugin::getObject,
                                 QCoreApplication::translate("PythonDocDataControl","Returns the id of an object with given name.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Name of an object").toLatin1().data());

  data.def("dataType",  &DataControlPlugin::dataType,
                                 QCoreApplication::translate("PythonDocDataControl","Returns the DataType of the object with the given id.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data());

  data.def("hideObject",  &DataControlPlugin::hideObject,
                                 QCoreApplication::translate("PythonDocDataControl","Hide object with the given id.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data());

  data.def("showObject",  &DataControlPlugin::showObject,
                                 QCoreApplication::translate("PythonDocDataControl","Show object with the given id.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data());

  data.def("setTarget",  &DataControlPlugin::setTarget,
                                 QCoreApplication::translate("PythonDocDataControl","Set given object as target.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Mark as target?").toLatin1().data());

  data.def("setSource",  &DataControlPlugin::setSource,
                                 QCoreApplication::translate("PythonDocDataControl","Set given object as source.").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Mark as target?").toLatin1().data());

  data.def("setObjectName",  &DataControlPlugin::setObjectName,
                                 QCoreApplication::translate("PythonDocDataControl","Set the name of the given object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","New name").toLatin1().data());

  data.def("setAllTarget",  &DataControlPlugin::setAllTarget,
                                 QCoreApplication::translate("PythonDocDataControl","Set All objects as targets").toLatin1().data());

  data.def("setAllSource",  &DataControlPlugin::setAllSource,
                                 QCoreApplication::translate("PythonDocDataControl","Set All objects as source").toLatin1().data());


  data.def("clearAllTarget",  &DataControlPlugin::clearAllTarget,
                                 QCoreApplication::translate("PythonDocDataControl","Clear targets").toLatin1().data());

  data.def("clearAllSource",  &DataControlPlugin::clearAllSource,
                                 QCoreApplication::translate("PythonDocDataControl","Clear sources").toLatin1().data());

  data.def("showAll",  &DataControlPlugin::showAll,
                                 QCoreApplication::translate("PythonDocDataControl","Show All objects").toLatin1().data());

  data.def("hideAll",  &DataControlPlugin::hideAll,
                                 QCoreApplication::translate("PythonDocDataControl","Hide all objects").toLatin1().data());

  data.def("addEmptyGroup",  &DataControlPlugin::addEmptyGroup,
                                 QCoreApplication::translate("PythonDocDataControl","Create new empty group. Returns the id of the new group.").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Name of the new group").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Parent group (Defaults to top level)").toLatin1().data() ) = 0) ;

  data.def("groupObjects",  &DataControlPlugin::groupObjects,
                                 QCoreApplication::translate("PythonDocDataControl","Group objects together. Return id of the new group or -1 on error").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","List of objects to be added to the group").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Name of the new group").toLatin1().data() ) = "") ;

  data.def("addObjectToGroup",  &DataControlPlugin::addObjectToGroup,
                                 QCoreApplication::translate("PythonDocDataControl","Add an Object to an existing group").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Id of an object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Group id").toLatin1().data() ) ) ;

  data.def("unGroupObject",  &DataControlPlugin::unGroupObject,
                                 QCoreApplication::translate("PythonDocDataControl","Remove the given object from its group and move it to the top level").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Id of an Object").toLatin1().data()) ) ;

  data.def("getTargetObjects",  &DataControlPlugin::getTargetObjects,
                                 QCoreApplication::translate("PythonDocDataControl","Returns the IdList of all target objects with given DataType.").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","DataType of the target objects").toLatin1().data()) ) ;

  data.def("getSourceObjects",  &DataControlPlugin::getSourceObjects,
                                  QCoreApplication::translate("PythonDocDataControl","Returns the IdList of all source objects with given DataType.").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocDataControl","DataType of the target objects").toLatin1().data()) ) ;

  data.def("groupCount",  &DataControlPlugin::groupCount,
                                 QCoreApplication::translate("PythonDocDataControl","Returns the number of groups").toLatin1().data() ) ;

  data.def("availableDataTypeNames",  &DataControlPlugin::availableDataTypeNames,
                                 QCoreApplication::translate("PythonDocDataControl","Returns a QStringList of all available DataType names.").toLatin1().data() ) ;

  data.def("printObjectInfoToLog",  &DataControlPlugin::printObjectInfoToLog,
                                 QCoreApplication::translate("PythonDocDataControl","Print information about all open objects to the console").toLatin1().data() ) ;

  data.def("getGroupElements",  &DataControlPlugin::getGroupElements,
                                 QCoreApplication::translate("PythonDocDataControl","Get all elements of the given group").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Id of a group").toLatin1().data()) ) ;

  data.def("showReducedUi",  &DataControlPlugin::showReducedUi,
                                 QCoreApplication::translate("PythonDocDataControl","Show or hide the extended ui interface in the datacontrol toolbox").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDataControl","Show?").toLatin1().data()) ) ;

}

